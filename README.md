# analysis_demo

This repository contains the code for the Analysis ML Demo during the October 2023 ATLAS week at CERN. The content of the demo is largely taken from [Top ML Tutorial](https://gitlab.cern.ch/atlasphys-top/reco/mltutorial/-/tree/main?ref_type=heads).

A Docker image will be produced by the git CI pipeline after each push and the container will be tagged as latest. Once produce, the Docker image can be found in the [gitlab registry](https://gitlab.cern.ch/jolamber/analysis_demo/container_registry)

To produce a singularity image from the Docker container, run
```
singularity pull docker://gitlab-registry.cern.ch/jolamber/analysis_demo/docker/training:latest
```
The image can be large so it is best to not pull the image in your home directory. Additionally, singularity will cache layers that it downloads. It is recommended to change the default cache directory using:
```
export SINGULARITY_CACHEDIR="/tmp/$(whoami)/singularity"
mkdir -p $SINGULARITY_CACHEDIR
``` 

If the repository is private, you will need to login with:
```
singularity remote login -u <user name> docker://gitlab-registry.cern.ch 
```
after replaceing `<user name>` with your CERN user name.

## Starting the Jupyter server

To start the Jupyter server, you run
```
singularity run training_latest.sif
```
or
```
singularity run training_latest.sif
    jupyter notebook \
    --no-browser \
    --ip=0.0.0.0 \
    --port=8888
```
This will start the singularity container and start the Jupyter server.


In a separate terminal
```
ssh -NL 8888:localhost:8888 <user name>@lxplusXXX.cern.ch

```
where `8888` is the port used by the Jupyter server and `XXX` corresponds to the lxplus instance you are using. Make sure that the port number that you use corresponds to what the Jupyter server is using.
