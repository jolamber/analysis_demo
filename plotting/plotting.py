import matplotlib.pyplot as plt


# Adjust some of the settings for matplotlib plotting.
plt.rcParams.update(
    {
        "figure.figsize": [8.0, 4.0],
        "xtick.major.width": 1.5,
        "xtick.major.size": 10.0,
        "xtick.minor.size": 5.0,
        "ytick.major.width": 1.5,
        "ytick.major.size": 10.0,
        "ytick.minor.size": 5.0,
        "font.size": 12,
        "lines.linewidth": 2.0,
    }
)


def create_loss_plot(fit_history, output_file: str = "loss.png"):
    """Creates a loss plot for training and validation data."""
    plt.figure()
    plt.plot(fit_history.history["loss"], label="training")
    plt.plot(fit_history.history["val_loss"], label="validation")
    plt.xlabel("Number of epochs")
    plt.ylabel("Loss value")
    plt.legend()
    plt.tight_layout()
    plt.savefig(output_file)


def create_accuracy_plot(fit_history, output_file: str = "accuracy.png"):
    """Creates an accuracy plot for training and validation data."""
    plt.figure()
    plt.plot(fit_history.history["binary_accuracy"], label="training")
    plt.plot(fit_history.history["val_binary_accuracy"], label="validation")
    plt.xlabel("Number of epochs")
    plt.ylabel("Accuracy value")
    plt.legend()
    plt.tight_layout()
    plt.savefig(output_file)

def create_nn_output_plot(
    model, x_train, x_test, y_train, y_test, output_file: str = "nn_output.png"
):
    """Creates a plot of the NN output for training and test data.

    The function slices both training and test data according to the truth
    labels, so that the displayed spectra can be split into "background" and
    "signal" events (i.e. the two classes the model was trained on).

    """
    plt.figure()
    _, bins, _ = plt.hist(
        model.predict(x_test[y_test.astype(bool)]),
        bins=20,
        alpha=0.3,
        density=True,
        label="test signal",
    )
    plt.hist(
        model.predict(x_test[~y_test.astype(bool)]),
        bins=bins,
        alpha=0.3,
        density=True,
        label="test bg",
    )
    plt.hist(
        model.predict(x_train[y_train.astype(bool)]),
        bins=bins,
        density=True,
        histtype="step",
        label="train signal",
    )
    plt.hist(
        model.predict(x_train[~y_train.astype(bool)]),
        bins=bins,
        density=True,
        histtype="step",
        label="train bg",
    )
    plt.xlabel("NN output")
    plt.legend()
    plt.tight_layout()
    plt.savefig(output_file)


def create_plot(x_sg, x_bg, name=''):
    plt.figure()
    _, bins, _ = plt.hist(
        x_sg,
        bins=20,
        #alpha=0.3,
        density=True,
        histtype="step",
        label="signal",
    )
    plt.hist(
        x_bg,
        bins=bins,
        #alpha=0.3,
        density=True,
        histtype="step",
        label="bg",
    )
    plt.xlabel(name)
    plt.legend()
    plt.tight_layout()
